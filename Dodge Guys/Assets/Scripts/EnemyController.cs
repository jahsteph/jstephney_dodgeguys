﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {


	//Speed of enemy
	public float speed = 8F;


	// Use this for initialization
	void Start () {
		
	}

	//Update is called once per frame
	void Update () {

		//movement of enemy
		transform.Translate (0, -speed * Time.deltaTime, 0);
	}

	void OnTriggerEnter2D(Collider2D other){

			//checks if other gameobject has a Tag of player

			if(other.gameObject.tag == "Player"){

				//pausees gameplay
				Time.timeScale = 0;
			}
	}
}